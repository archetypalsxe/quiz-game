FROM ruby:2.3.3

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
WORKDIR /usr/src/app
COPY quizServer/Gemfile* ./
RUN bundle install
COPY quizServer/ .

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
